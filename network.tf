resource "aws_security_group" "alb" {
  name = "${var.name_prefix}-alb-sg"
  vpc_id = "${aws_vpc.jenkins-vpc.id}"

  ingress {
    from_port = 443
    to_port = 443
    protocol = "tcp"
    #166.70.94.106/32:erik@folium, 67.161.6.118/32:steve@folium, 207.140.43.0/24:samsung-ssic
    #[104.192.136.0/21, 34.198.203.127/32, 34.198.178.64/32, 34.198.32.85/32]:bitbucket-webhooks
    cidr_blocks = [ "166.70.94.106/32", "67.161.6.118/32", "207.140.43.0/24", "104.192.136.0/21", "34.198.203.127/32", "34.198.178.64/32", "34.198.32.85/32" ]
    ipv6_cidr_blocks = ["2607:fa18:3a01::/48", "2601:647:4100:4ad9::/64"]
  }

  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
  }

}

resource "aws_security_group" "spot-fleet" {
  name = "${var.name_prefix}-spot-fleet-sg"
  vpc_id = "${aws_vpc.jenkins-vpc.id}"

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [ "${var.private_subnet_cidrs}" ]
    ipv6_cidr_blocks = [ "2607:fa18:3a01::/48" ]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }
  
  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmpv6"
    ipv6_cidr_blocks = [ "::/0" ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
  }
}

resource "aws_security_group" "jenkins-sg" {
  name = "${var.name_prefix}-sg"
  vpc_id = "${aws_vpc.jenkins-vpc.id}"

  ingress { 
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [ "${var.private_subnet_cidrs}" ]
    ipv6_cidr_blocks = [ "2607:fa18:3a01::/48", "${aws_vpc.jenkins-vpc.ipv6_cidr_block}" ]
  }
  
  ingress { 
    from_port = 8080
    to_port = 8080
    protocol = "tcp"
    cidr_blocks = [ "${var.private_subnet_cidrs}", "${var.public_subnet_cidrs}" ]
    ipv6_cidr_blocks = [ "${aws_vpc.jenkins-vpc.ipv6_cidr_block}", "2607:fa18:3a01::/48" ]
  }

  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    cidr_blocks = [ "${var.private_subnet_cidrs}" ]
  }
  
  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmpv6"
    ipv6_cidr_blocks = [ "${aws_vpc.jenkins-vpc.ipv6_cidr_block}", "2607:fa18:3a01::/48" ]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
  }
}

resource "aws_vpc" "jenkins-vpc" {
  cidr_block = "192.168.180.0/24"
  assign_generated_ipv6_cidr_block = true
  enable_dns_hostnames = true
  tags {
    Name = "${var.name_prefix}-vpc"
  }
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "private_0" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = "192.168.180.0/26"
  ipv6_cidr_block = "${cidrsubnet("${aws_vpc.jenkins-vpc.ipv6_cidr_block}",8,0)}"
  assign_ipv6_address_on_creation = true
  tags {
    Name = "${var.name_prefix}-private-${data.aws_availability_zones.available.names[0]}"
  }
}

resource "aws_subnet" "private_1" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = "192.168.180.64/26"
  ipv6_cidr_block = "${cidrsubnet("${aws_vpc.jenkins-vpc.ipv6_cidr_block}",8,1)}"
  assign_ipv6_address_on_creation = true
  tags {
    Name = "${var.name_prefix}-private-${data.aws_availability_zones.available.names[1]}"
  }
}

resource "aws_subnet" "public_0" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[0]}"
  cidr_block = "192.168.180.128/26"
  ipv6_cidr_block = "${cidrsubnet("${aws_vpc.jenkins-vpc.ipv6_cidr_block}",8,2)}"
  assign_ipv6_address_on_creation = true
  tags {
    Name = "${var.name_prefix}-public-${data.aws_availability_zones.available.names[0]}"
  }
}

resource "aws_subnet" "public_1" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  availability_zone = "${data.aws_availability_zones.available.names[1]}"
  cidr_block = "192.168.180.192/26"
  ipv6_cidr_block = "${cidrsubnet("${aws_vpc.jenkins-vpc.ipv6_cidr_block}",8,3)}"
  assign_ipv6_address_on_creation = true
  tags {
    Name = "${var.name_prefix}-public-${data.aws_availability_zones.available.names[1]}"
  }
}

resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  tags {
    Name = "${var.name_prefix}-gw"
  }
}

resource "aws_vpc_endpoint" "s3" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  service_name = "com.amazonaws.${var.region}.s3"
  route_table_ids = ["${aws_route_table.private.id}"]
}

resource "aws_default_route_table" "default-route" {
  default_route_table_id = "${aws_vpc.jenkins-vpc.default_route_table_id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }

  route {
    ipv6_cidr_block = "::/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
  tags {
    Name = "${var.name_prefix}-route"
  }
}

resource "aws_eip" "nat-eip" {
  vpc = true
  tags {
    Name = "${var.name_prefix}-eip"
  }
}

resource "aws_nat_gateway" "nat-gw" {
  allocation_id = "${aws_eip.nat-eip.id}"
  subnet_id = "${aws_subnet.public_0.id}"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat-gw.id}"
  }
  route {
    ipv6_cidr_block = "::/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
  tags {
    Name = "${var.name_prefix}-private-route"
  }
}

resource "aws_route_table_association" "private_0" {
  subnet_id = "${aws_subnet.private_0.id}"
  route_table_id = "${aws_route_table.private.id}"
}

resource "aws_route_table_association" "private_1" {
  subnet_id = "${aws_subnet.private_1.id}"
  route_table_id = "${aws_route_table.private.id}"
}
