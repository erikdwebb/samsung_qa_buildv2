variable "aws_access_key" {
  default = ""
}

variable "aws_secret_key" {
  default = ""
}

variable "region" {
  default = ""
  description = "The region of AWS, for AMI lookups"
}

variable "name_prefix" {
  default = "cloud-jenkins"
  description = "Prefix for all AWS resource names"
}

variable "domain_name" {
  default = ""
  description = "Domain suffix. example: yourdomain.com"
}

variable "route53_zone_id" {
  default = ""
  description = "AWS route53 zone id, currently used in Cert generation"
}

variable "key_name" {
  description = "SSH key name in your AWS account for AWS instances"
}

variable "instance_type" {
  default = "t2.medium"
  description = "AWS Instance type"
}

variable "root_volume_size" {
  default = "16"
  description = "Size in GB of the root volume for instances"
}

variable "vpc_cidr" {
  default = "192.168.0.0/22"
  description = "Subnet in CIDR format to assign to VPC"
}

variable "private_subnet_cidrs" {
  type = "list"
  default = [
    "192.168.0.0/26",
    "192.168.0.64/26"
  ]
  description = "Subnet ranges"
}

variable "public_subnet_cidrs" {
  type = "list"
  default = [
    "192.168.0.128/26",
    "192.168.0.192/26"
  ]
  description = "Subnet ranges"
}

variable "ecs_ami" { 
  type = "string" 
  default = ""
}

variable "spot_fleet_ami" {
  type = "string"
  default = ""
}

variable "vpc_domain_name" {
  default = "ec2.internal"
  description = "Internal DNS domain name"
}

variable "spot_fleet_valid_until" {
  type = "string"
  default = "2019-08-22T02:50:19Z"
}

