resource "aws_spot_fleet_request" "jenkins-fleet" {
  iam_fleet_role      = "arn:aws:iam::379419836112:role/aws-ec2-spot-fleet-tagging-role"
  allocation_strategy = "lowestPrice"
  target_capacity     = 1
  valid_until         = "${var.spot_fleet_valid_until}"
  lifecycle {
    ignore_changes = ["*"]
  }
  launch_specification {
    vpc_security_group_ids    = ["${aws_security_group.spot-fleet.id}"]
    ami                       = "${var.spot_fleet_ami}"
    instance_type             = "c5.xlarge"
    key_name                  = "${var.key_name}"
    subnet_id                 = "${aws_subnet.private_0.id}, ${aws_subnet.private_1.id}"
    root_block_device {
      volume_size = "64"
    }
    tags {
      Name = "${var.name_prefix}-spotfleet"
    }
  }
}
