aws_access_key = ""
aws_secret_key = ""
key_name = "qa-builder-v2"
name_prefix = "qa-jenkins"
domain_name = "ssic.io"
region = "us-east-2"
private_subnet_cidrs = [
  "192.168.180.0/26",
  "192.168.180.64/26"
]
public_subnet_cidrs = [
  "192.168.180.128/26",
  "192.168.180.192/26"
]
vpc_cidr = "192.168.180.0/24"
spot_fleet_valid_until = "2019-08-22T02:50:19Z"
route53_zone_id = "Z3IHY9RDH8ZDYM"
ecs_ami = "ami-028a9de0a7e353ed9"
spot_fleet_ami = "ami-0e9917707ecde2d6c"

