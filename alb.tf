resource "aws_alb" "ecs-load-balancer" {
    name                = "${var.name_prefix}-ecs-load-balancer"
    security_groups     = ["${aws_security_group.alb.id}"]
    subnets             = [ "${aws_subnet.public_0.id}", "${aws_subnet.public_1.id}" ]

    tags {
      Name = "${var.name_prefix}-ecs-load-balancer"
    }
}

resource "aws_alb_target_group" "ecs-target-group" {
    name                = "${var.name_prefix}-ecs-target-group"
    port                = "8080"
    protocol            = "HTTP"
    vpc_id              = "${aws_vpc.jenkins-vpc.id}"

    health_check {
        healthy_threshold   = "5"
        unhealthy_threshold = "2"
        interval            = "30"
        matcher             = "200"
        path                = "/login"
        port                = "traffic-port"
        protocol            = "HTTP"
        timeout             = "5"
    }

    tags {
      Name = "${var.name_prefix}-ecs-target-group"
    }
}

#resource "aws_alb_listener" "alb-listener" {
#    load_balancer_arn = "${aws_alb.ecs-load-balancer.arn}"
#    port              = "80"
#    protocol          = "HTTP"
#
#    default_action {
#        target_group_arn = "${aws_alb_target_group.ecs-target-group.arn}"
#        type             = "forward"
#    }
#}


resource "aws_alb_listener" "alb-listener-ssl" {
    load_balancer_arn = "${aws_alb.ecs-load-balancer.arn}"
    port              = "443"
    protocol          = "HTTPS"
    certificate_arn = "${aws_acm_certificate_validation.jenkins-cert.certificate_arn}"
    default_action {
       target_group_arn = "${aws_alb_target_group.ecs-target-group.arn}"
       type             = "forward"
   }
}

resource "aws_lb_listener" "alb-redirect-to-https" {
  load_balancer_arn = "${aws_alb.ecs-load-balancer.arn}"
  protocol          = "HTTP"
  port              = "80"

  default_action {
    type = "redirect"
    redirect {
      port = "443"
      protocol = "HTTPS"
      status_code = "HTTP_301"
    }
  }
}
