provider "aws" {
  access_key = "${var.aws_access_key}"
  secret_key = "${var.aws_secret_key}"
  region = "us-east-2"
  version = "~> 1.33"
}

terraform {
  backend "s3" {
    bucket = "ssic-terraform"
    key = "us-east-2/qa-jenkins"
    region = "us-east-2"
  }
}
