resource "aws_acm_certificate" "jenkins-cert" {
  domain_name = "${var.name_prefix}.${var.domain_name}"
  validation_method = "DNS"
}

resource "aws_route53_record" "cert_validation" {
  name = "${aws_acm_certificate.jenkins-cert.domain_validation_options.0.resource_record_name}"
  type = "${aws_acm_certificate.jenkins-cert.domain_validation_options.0.resource_record_type}"
  zone_id = "${var.route53_zone_id}"
  records = ["${aws_acm_certificate.jenkins-cert.domain_validation_options.0.resource_record_value}"]
  ttl = 60
}

resource "aws_acm_certificate_validation" "jenkins-cert" {
  certificate_arn = "${aws_acm_certificate.jenkins-cert.arn}"
  validation_record_fqdns = ["${aws_route53_record.cert_validation.fqdn}"]
}
