resource "aws_launch_configuration" "jenkins-ecs-lc" {
  name = "${var.name_prefix}-jenkins-ecs-lc"
  image_id = "${var.ecs_ami}"
  instance_type = "${var.instance_type}"
  key_name = "${var.key_name}"
  associate_public_ip_address = false
  user_data = <<EOF
#!/bin/bash
echo ECS_CLUSTER="${aws_ecs_cluster.ecs_cluster.name}" >> /etc/ecs/ecs.config 
yum -y install nfs-utils
mkdir /mnt/efs
chown nobody:docker /mnt/efs
mount -t nfs ${aws_efs_file_system.jenkins-efs.dns_name}: /mnt/efs
EOF
 security_groups = [ "${aws_security_group.jenkins-sg.id}" ]
 iam_instance_profile = "${aws_iam_instance_profile.ecs-instance-profile.id}"
}

resource "aws_autoscaling_group" "jenkins-asg" {
  name = "${var.name_prefix}-asg"
  max_size = 1
  min_size = 1
  desired_capacity = 1
  launch_configuration = "${aws_launch_configuration.jenkins-ecs-lc.name}"
  vpc_zone_identifier = ["${aws_subnet.private_0.id}", "${aws_subnet.private_1.id}"]
  tags {
    key = "Name"
    value = "${var.name_prefix}-ecs-member"
    propagate_at_launch = "true"
  }
}

resource "aws_ecs_cluster" "ecs_cluster" {
  name = "${var.name_prefix}"
}

resource "aws_ecs_task_definition" "jenkins" {
  family = "jenkins-task"
  container_definitions = "${file("jenkins-service.json")}"

  volume {
    name = "jenkins-efs"
    host_path = "/mnt/efs/jenkins_home"
  }
}

resource "aws_ecs_service" "jenkins" {
  name = "jenkins-service"
  cluster = "${aws_ecs_cluster.ecs_cluster.id}"
  task_definition = "${aws_ecs_task_definition.jenkins.arn}"
  desired_count = 1
  load_balancer {
    target_group_arn = "${aws_alb_target_group.ecs-target-group.arn}"
    container_port = 8080
    container_name = "jenkins"
  }
}

