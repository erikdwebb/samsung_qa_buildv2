resource "aws_s3_bucket" "artifact_bucket" {
  bucket = "${var.name_prefix}-artifacts"
  acl = "private"
  tags {
    Name = "${var.name_prefix}-artifacts"
  }
}
