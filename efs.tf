resource "aws_efs_file_system" "jenkins-efs" {
    tags {
        Name = "${var.name_prefix}-efs"
    }
    lifecycle { 
      prevent_destroy = true
    }
}

resource "aws_efs_mount_target" "jenkins-efs-mount-target-0" {
   file_system_id  = "${aws_efs_file_system.jenkins-efs.id}"
   subnet_id       = "${aws_subnet.private_0.id}"
   security_groups = ["${aws_security_group.jenkins-efs.id}"]
}

resource "aws_efs_mount_target" "jenkins-efs-mount-target-1" {
   file_system_id  = "${aws_efs_file_system.jenkins-efs.id}"
   subnet_id       = "${aws_subnet.private_1.id}"
   security_groups = ["${aws_security_group.jenkins-efs.id}"]
}

resource "aws_security_group" "jenkins-efs" {
  name = "${var.name_prefix}-efs-sg"
  description = "Jenkins EFS security group."
  ingress {
    from_port = 2049
    to_port = 2049
    protocol = "tcp"
    cidr_blocks = [ "${var.private_subnet_cidrs}" ]
    ipv6_cidr_blocks = [ "${aws_vpc.jenkins-vpc.ipv6_cidr_block}" ]
  }
  ingress {
    from_port = "-1"
    to_port = "-1"
    protocol = "icmp"
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
  }
  egress {
    from_port = "0"
    to_port = "0"
    protocol = "-1"
    cidr_blocks = [ "0.0.0.0/0" ]
    ipv6_cidr_blocks = [ "::/0" ]
  }
  vpc_id = "${aws_vpc.jenkins-vpc.id}"
  tags {
    Terraform = "true"
    Name = "${var.name_prefix}-efs-sg"
  }
}
